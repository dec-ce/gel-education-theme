(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jquery", "utilities/anchorFilter", "utilities/scrollToggle"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jquery"), require("utilities/anchorFilter"), require("utilities/scrollToggle"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jquery, global.anchorFilter, global.scrollToggle);
    global.anchorHighlight = mod.exports;
  }
})(this, function (_exports, $, _anchorFilter, _scrollToggle) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  $ = _interopRequireWildcard($);
  _anchorFilter = _interopRequireDefault(_anchorFilter);
  _scrollToggle = _interopRequireDefault(_scrollToggle);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var AnchorHighlight = function AnchorHighlight(selector) {
    _classCallCheck(this, AnchorHighlight);

    this.scroll_config = {
      "subject": "[data-character-name] a",
      "scroller_position": "percent",
      "scroller_percent": 5,
      "linked": true,
      "scroll_window": true,
      "toggle_class": "active"
    }; // Instantiate the AnchorFilter

    this.anchor = new _anchorFilter["default"](selector); //Remove unneeded attributes

    $('span').parent().removeAttr('data-character-name'); // Instantiate the ScrollToggle

    this.scroll = new _scrollToggle["default"](selector, this.scroll_config); //IE fix

    if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/)) || typeof $.browser !== "undefined" && $.browser.msie == 1) {
      $("li a[href*='#Q']").each(function () {
        $(this).attr('href', '#Qu');
      });
      $("li span[id*='Q']").each(function () {
        $(this).attr('id', 'Qu');
      });
    } // Highlight bottom letters that wont be triggered by scrollToggle


    $('.gel-a-z-anchors li a').click(function () {
      var _this = $(this);

      window.onscroll = function (ev) {
        if (_this != null) {
          $('.gel-a-z-anchors li a').removeClass('active');

          _this.addClass('active');

          _this = null;
        } else {
          _this = null;
        }
      };

      if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight) {
        if (_this != null) {
          $('.gel-a-z-anchors li a').removeClass('active');

          _this.addClass('active');
        } else {
          _this = null;
        }
      }
    });
  };

  var _default = AnchorHighlight;
  _exports["default"] = _default;
});