(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.AggregatorFilter = mod.exports;
  }
})(this, function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  var AggregatorFilter =
  /*#__PURE__*/
  function () {
    function AggregatorFilter(root, attributeName) {
      var _this = this;

      _classCallCheck(this, AggregatorFilter);

      this.root = root;
      this.attributeName = attributeName;
      this.filterWrapper = root.querySelector('.gel-content-aggregator__collapsible-wrapper.filter-content');
      this.optionsWrapper = root.querySelector('.gel-content-aggregator__collapsible-wrapper.filter-options');
      this.filterValues = [];

      if (this.optionsWrapper) {
        this.calculateOptionsHeight();
      }

      if (this.filterWrapper) {
        this.filterContent = this.filterWrapper.querySelector('.wrapper-content');
        this.expand(this.root.classList.contains('expanded'));
        root.querySelector('.filter-title').addEventListener('click', function (evt) {
          evt.preventDefault();

          _this.expand(!_this.root.classList.contains('expanded'));
        });
      }

      if (root.querySelector('.more-options')) {
        root.querySelector('.more-options').addEventListener('click', function (evt) {
          evt.preventDefault();

          var optionsContent = _this.optionsWrapper.querySelector('.wrapper-content');

          var filterHeight = _this.filterWrapper.querySelector('.wrapper-content').clientHeight;

          _this.optionsWrapper.classList.add('expanded');

          _this.optionsWrapper.setAttribute('style', "max-height: ".concat(optionsContent.clientHeight, "px"));

          filterHeight = filterHeight + (optionsContent.clientHeight - _this.collapsedOptionsHeight);

          _this.filterWrapper.setAttribute('style', "max-height: ".concat(filterHeight, "px"));
        });
      }

      if (root.querySelector('.less-options')) {
        root.querySelector('.less-options').addEventListener('click', function (evt) {
          evt.preventDefault();

          _this.optionsWrapper.setAttribute('style', "max-height:".concat(_this.collapsedOptionsHeight, "px"));

          _this.optionsWrapper.classList.remove('expanded');
        });
      }

      var filterOptions = root.querySelector('.filter-options');

      if (filterOptions) {
        filterOptions.addEventListener('change', function (evt) {
          var filterValue = evt.target.parentElement.querySelector('input').getAttribute('value');

          var valueIndex = _this.filterValues.indexOf(filterValue);

          if (valueIndex > -1) {
            _this.filterValues.splice(valueIndex, 1);
          } else {
            _this.filterValues.push(filterValue);
          }

          _this.root.dispatchEvent(new CustomEvent('filterChanged', {
            detail: {
              attributeName: _this.attributeName,
              filterValues: _this.filterValues
            }
          }));
        });
      }
    }

    _createClass(AggregatorFilter, [{
      key: "calculateOptionsHeight",
      value: function calculateOptionsHeight() {
        this.collapsedOptionsHeight = 0;
        var options = this.root.querySelectorAll('.option');

        for (var i = 0; i < Math.min(5, options.length); i++) {
          this.collapsedOptionsHeight += options[i].clientHeight + 8;
        }

        this.optionsWrapper.setAttribute('style', "max-height:".concat(this.collapsedOptionsHeight, "px"));
        return this.collapsedOptionsHeight;
      }
    }, {
      key: "isExpanded",
      value: function isExpanded() {
        return this.root.classList.contains('expanded');
      }
    }, {
      key: "expand",
      value: function expand(_expand) {
        if (!_expand) {
          this.filterWrapper.setAttribute('style', 'max-height:0px');
          this.root.classList.remove('expanded');
        } else {
          this.filterWrapper.setAttribute('style', "max-height: ".concat(this.filterContent.clientHeight, "px"));
          this.root.classList.add('expanded');
        }
      }
    }, {
      key: "setFilterValues",
      value: function setFilterValues(filterValues) {
        Array.from(this.root.querySelectorAll('input[type="checkbox"]')).forEach(function (checkbox) {
          checkbox.checked = filterValues.indexOf(checkbox.value) > -1;
        });
        this.filterValues = filterValues;
      }
    }, {
      key: "shouldInclude",
      value: function shouldInclude(item) {
        var _this2 = this;

        var attributeValue = item[this.attributeName];

        if (Array.isArray(attributeValue)) {
          return item[this.attributeName].reduce(function (result, tag) {
            return result || _this2.filterValues.indexOf(tag) > -1;
          }, false);
        } else {
          return this.filterValues.indexOf(item[this.attributeName]) > -1;
        }
      }
    }, {
      key: "isActive",
      value: function isActive() {
        return this.filterValues.length > 0;
      }
    }]);

    return AggregatorFilter;
  }();

  var _default = AggregatorFilter;
  _exports["default"] = _default;
});