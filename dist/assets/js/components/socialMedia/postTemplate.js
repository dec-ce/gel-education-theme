(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.postTemplate = mod.exports;
  }
})(this, function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  var postTemplate = "\n{{? !it.post.service.match(/error/) }}\n  <li>\n    <article class=\"gel-card gel-social-media__card\">\n      <div class=\"gel-card__header gel-social-media__header\">\n        <div class=\"gel-social-media__avatar\">\n          <img src=\"{{=it.post.user.avatar}}\" alt=\"\" />\n        </div>\n\n        <div class=\"gel-social-media__name\">\n          {{=it.post.user.name}}\n        </div>\n        {{? it.post.service.match(/twitter/) }}\n          <div class=\"gel-social-media__username\">\n            @{{=it.post.user.username}}\n          </div>\n        {{?}}            \n      </div>\n\n      {{? it.post.service.match(/youtube|vimeo/)}}\n        <div class=\"gel-card__media\">\n          <iframe  width=\"360\" height=\"203\" title=\"{{=it.post.id}} video\" frameborder=\"0\" src=\"{{=it.post.embedUrl()}}\" frameborder=\"0\" allowfullscreen ></iframe>\n        </div>\n      {{?}}\n\n      {{? it.post.content.photo && !it.post.service.match(/youtube|vimeo/)}}\n        <div class=\"gel-card__media\">\n          <img src=\"{{=it.post.content.photo}}\" alt=\"\" />\n        </div>\n      {{?}}\n\n      <div class=\"gel-card__content\">\n        {{? it.post.content.text}}\n          <section class=\"gel-social-media__content\">{{=it.post.content.text}}</section>\n        {{?}}\n\n        <p>\n          <a href=\"{{=it.post.url}}\" class=\"gel-social-media__post-link\">\n            View post on {{=it.post.service.capitalize()}}<span class=\"gel-external-link\"></span>\n          </a>\n        </p>\n      </div>\n\n      <div class=\"gel-card__footer\">\n        <a href={{=it.post.user.url}} class=\"gel-social-media__footer-link gel-remove-external-link\">\n          <i class=\"fab fa-{{=it.post.service}}\" aria-hidden=\"true\" aria-label=\"{{=it.post.service}} icon\"></i>\n          <span class=\"show-on-sr\">posted</span>{{=it.moment(it.post.published).fromNow()}}\n        </a>\n      </div>\n    </article>\n  </li>\n{{?}}\n";
  var _default = postTemplate;
  _exports["default"] = _default;
});