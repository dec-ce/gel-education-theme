(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "moment", "dot", "./postTemplate"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("moment"), require("dot"), require("./postTemplate"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.moment, global.dot, global.postTemplate);
    global.post = mod.exports;
  }
})(this, function (_exports, _moment, _dot, _postTemplate) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  _moment = _interopRequireDefault(_moment);
  _dot = _interopRequireDefault(_dot);
  _postTemplate = _interopRequireDefault(_postTemplate);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  /**
   * Stores social media post data
   *
   * @since 1.1.31
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   *
   * @requires momment:vendor/moment/moment
   */
  var Post =
  /*#__PURE__*/
  function () {
    /**
     * Post constructor
     *
     * @constructor
     *
     * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
     * @param {boolean} posttruncate
     * @example
     * // Instantiate a new Post
     * let Post = new Post(rawPost)
     */
    function Post(rawPost) {
      _classCallCheck(this, Post);

      if (rawPost) {
        this.processRawPost(rawPost);
      }
    }
    /**
     * Takes a raw social media post returned by the Social Media Aggregator and maps it to Post properties
     *
     * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
     */


    _createClass(Post, [{
      key: "processRawPost",
      value: function processRawPost(rawPost) {
        this.rawPost = rawPost;
        this.service = rawPost.accountType;
        this.user = {
          username: rawPost.username,
          name: rawPost.screenName,
          avatar: rawPost.avatarUrl,
          url: rawPost.userUrl
        };
        this.published = rawPost.rawTimestamp;
        this.content = {
          photo: rawPost.thumbnail,
          text: rawPost.text
        };
        this.url = rawPost.url; // If the post doesn't already have an ID then in the case of YouTube and Vimeo posts use the video ID as the post ID

        if (rawPost.postId) {
          this.id = rawPost.postId;
        } else if (rawPost.accountType == "vimeo" || rawPost.accountType == "youtube") {
          this.id = this.extractVideoID(rawPost.url);
        }
      }
      /**
       * Takes a Vimeo or YouTube video URL and returns the video ID
       *
       * @param {String} url - A video URL
       *
       * @returns {String} - A YouTube or Vimeo video ID
       */

    }, {
      key: "extractVideoID",
      value: function extractVideoID(url) {
        // If the URL is from YouTube
        if (url.match(/youtube/)) {
          return url.match(/\?v=([^&]*)/)[1];
        } // If the URL is from Vimeo


        if (url.match(/vimeo/)) {
          return url.match(/\https:\/\/vimeo.com\/([^&]*)/)[1];
        }
      }
      /**
       * Returns a YouTube/Vimeo iframe embed code compatible URL
       *
       * @returns {String} - The URL needed by the YouTube/Vimeo iframe embed code
       */

    }, {
      key: "embedUrl",
      value: function embedUrl() {
        // If the URL is from YouTube
        if (this.url.match(/youtube/)) {
          return "https://www.youtube.com/embed/".concat(this.id, "?rel=0");
        } // If the URL is from Vimeo


        if (this.url.match(/vimeo/)) {
          return "https://player.vimeo.com/video/".concat(this.id, "?rel=0");
        }
      }
      /**
       * Return's a Post as HTML string that conforms to the gel Social Media Post component spec
       *
       * @param {Number} contentLength - (optional) The number of characters the text content of the post should be trimed to
       * @param {Boolean} minimalist - (optional) Set to true to hide the majority of post metadata
       * @param {Boolean} dateplacement - (optional) Set to true to have a differet date design placement
       *
       * @returns {String} - Post as a HTML conforming to the gel Social Media Post component
       */

    }, {
      key: "toHTML",
      value: function toHTML(contentLength, minimalist, dateplacement) {
        var post = this; // If contentLength is specified then reduce the size of the text content

        if (post.content.text) {
          var posttruncate;

          if (contentLength) {
            var urlwrapinpost = function urlwrapinpost(poststr) {
              return '<a href="' + poststr + '">' + poststr + '</a>';
            }; //add truncate style only if the post is truncated


            // If contentLength is specified then reduce the size of the text content
            if (post.content.text.length > contentLength) {
              posttruncate = true;
            }

            post.content.text = post.content.text.substring(0, contentLength); //find if text has url if yes then wrap ahref around it

            post.content.text = post.content.text.replace(/\bhttp[^ ]+/ig, urlwrapinpost);

            if (posttruncate) {
              post.content.text = post.content.text.substr(0, Math.min(post.content.text.length, post.content.text.lastIndexOf(" ")));
              post.content.text = post.content.text.replace(/\s*$/, "") + "\u2026<span class=\"show-on-sr\">post truncated</span>";
            }
          }
        }

        if (post.service) {
          if (String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
          }) ;
        }

        var templateFn = _dot["default"].template(_postTemplate["default"]);

        var mediaHTML = templateFn({
          post: post,
          moment: _moment["default"],
          minimalist: minimalist,
          dateplacement: dateplacement
        });
        return mediaHTML;
      }
      /**
       * Returns a string representation of the SocialMediaAccount object
       *
       * @returns {String} - Post object as JSON string
       */

    }, {
      key: "toString",
      value: function toString() {
        return JSON.stringify(this);
      }
    }]);

    return Post;
  }();
  /**
   * Exports the Post class as a module
   * @module
   */


  var _default = Post;
  _exports["default"] = _default;
});