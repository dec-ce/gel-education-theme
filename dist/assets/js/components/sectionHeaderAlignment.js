(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define([], factory);
  } else if (typeof exports !== "undefined") {
    factory();
  } else {
    var mod = {
      exports: {}
    };
    factory();
    global.sectionHeaderAlignment = mod.exports;
  }
})(this, function () {
  "use strict";

  jQuery(document).ready(myfunction);
  jQuery(window).on('resize', myfunction);

  function myfunction() {
    var sectionHeaderHeight = $(".gel-section-header").outerHeight();
    var searchBox = $("#global-search");
    searchBox.css('height', sectionHeaderHeight);
  }
});