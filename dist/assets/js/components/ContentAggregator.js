(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["./AggregatorFilter"], factory);
  } else if (typeof exports !== "undefined") {
    factory(require("./AggregatorFilter"));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.AggregatorFilter);
    global.ContentAggregator = mod.exports;
  }
})(this, function (_AggregatorFilter) {
  "use strict";

  _AggregatorFilter = _interopRequireDefault(_AggregatorFilter);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  var ITEMS_PER_PAGE = 10;
  var MAX_PAGE_LINKS = 6;

  function toggleClass(element, className, state) {
    if (state) {
      element.classList.add(className);
    } else {
      element.classList.remove(className);
    }
  }

  var ContentAggregator =
  /*#__PURE__*/
  function () {
    function ContentAggregator(componentRoot, searchResult) {
      var _this = this;

      _classCallCheck(this, ContentAggregator);

      this.root = componentRoot;
      this.searchResult = searchResult;
      this.filteredResults = [];
      var templateNode = componentRoot.querySelector("script[rel='itemTemplate']");

      if (templateNode) {
        this.itemTemplate = templateNode.textContent;
      }

      this.filters = componentRoot.querySelectorAll('.gel-content-aggregator__filter');
      this.resultSummaryNode = componentRoot.querySelector('.results-summary p');
      this.resultItemsNode = componentRoot.querySelector('.gel-result-items');
      this.paginationNode = componentRoot.querySelector('.gel-pagination');
      this.applyFilterBtn = componentRoot.querySelector('.applyFilter');
      this.page = 0;
      this.pageCount = 0;
      this.filters = Array.from(this.filters).map(function (filterElement) {
        var filterAttributeName = filterElement.getAttribute('data-filter-attribute');
        filterElement.addEventListener('filterChanged', function (evt) {
          _this.filters.forEach(function (filter) {
            if (filter.attributeName === evt.detail.attributeName && filter.root !== evt.target) {
              filter.setFilterValues(evt.detail.filterValues);
            }
          });

          if (filterElement.getAttribute('data-auto-update') === 'true') {
            _this.update();
          }
        });
        return new _AggregatorFilter["default"](filterElement, filterAttributeName);
      });
      this.applyFilterBtn.addEventListener('click', this.onApplyFilterButtonClicked.bind(this));
      this.paginationNode.addEventListener('click', this.onPaginationClicked.bind(this));
      this.update();
    }

    _createClass(ContentAggregator, [{
      key: "update",
      value: function update() {
        var _this2 = this;

        this.page = 0;
        var isFiltering = this.filters.map(function (filter) {
          return filter.isActive();
        }).reduce(function (result, current) {
          return result || current;
        }, false);

        if (isFiltering) {
          this.filteredResults = this.searchResult.items.reduce(function (result, item) {
            // Filter objects are duplicated due to having a separate DOM elements for mobile, thus we only need to evaluate
            // half the filters
            for (var i = 0; i < _this2.filters.length / 2; i++) {
              var filter = _this2.filters[i];

              if (result.indexOf(item) === -1 && filter.shouldInclude(item)) {
                result.push(item);
                break;
              }
            }

            return result;
          }, []);
        } else {
          this.filteredResults = this.searchResult.items;
        }

        this.renderResult(this.filteredResults, this.page, ITEMS_PER_PAGE);
      }
    }, {
      key: "renderResult",
      value: function renderResult(items, page, itemsPerPage) {
        this.resultItemsNode.innerHTML = '';
        var offset = page * itemsPerPage;

        for (var i = offset; i < offset + Math.min(itemsPerPage, items.length - offset); i++) {
          var item = items[i];
          this.renderItem(item);
        }

        this.updatePagination(items, page, itemsPerPage);
        this.renderSummary(this.filteredResults, page);
        this.root.querySelector('.result-title').scrollIntoView(true);
      }
    }, {
      key: "renderItem",
      value: function renderItem(item) {
        if (!this.itemTemplate) {
          return;
        }

        var newElement = document.createElement('div');
        newElement.innerHTML = this.itemTemplate.trim();
        newElement.querySelector('.card-title').innerText = item.title;
        newElement.querySelector('a').setAttribute('href', item.contentPath);
        newElement.querySelector('a').setAttribute('title', item.title);
        newElement.querySelector('a').setAttribute('aria-label', item.title);
        var imageNode = newElement.querySelector('img');

        if (item.imagePath) {
          imageNode.setAttribute('src', item.imagePath);
          imageNode.setAttribute('alt', item.imageAltText);
        } else {
          imageNode.parentNode.removeChild(imageNode); // Because IE doesn't support node.remove()

          newElement.querySelector('.card-body-content').classList.remove('col-md-6');
        }

        if (item.isVideo) {
          var cardType = newElement.querySelector('.card-type');
          cardType.innerHTML = "<span class=\"fas fa-play-circle\" aria-hidden\"></span>\n                            <span>Video</span>";
        }

        if (item.isPodcast) {
          var _cardType = newElement.querySelector('.card-type');

          _cardType.innerHTML = "<span class=\"fas fa-podcast\" aria-hidden\"></span>\n                            <span>Podcast</span>";
        }

        var itemNode = newElement.firstChild;
        this.resultItemsNode.appendChild(itemNode);
      }
    }, {
      key: "renderSummary",
      value: function renderSummary(items, page) {
        var startIndex = page * ITEMS_PER_PAGE + (items.length > 0 ? 1 : 0);
        var endIndex = items.length > 0 ? startIndex + Math.min(items.length, ITEMS_PER_PAGE) - 1 : 0;
        this.resultSummaryNode.innerText = "Showing ".concat(startIndex, " - ").concat(endIndex, " of ").concat(items.length, " ").concat(items.length > 1 ? 'pages' : 'page');
      }
    }, {
      key: "updatePagination",
      value: function updatePagination(items, page, itemsPerPage) {
        this.page = page;
        Array.from(this.paginationNode.querySelectorAll('.gel-pagination__page')).forEach(function (pageLink) {
          return pageLink.parentNode.removeChild(pageLink);
        });
        this.pageCount = Math.ceil(items.length / itemsPerPage);

        if (this.pageCount <= 1) {
          this.paginationNode.classList.add('d-none');
          return;
        }

        var prevLink = this.paginationNode.querySelector('.gel-pagination__prev');
        var prevMore = this.paginationNode.querySelector('.gel-pagination__prevMore');
        var nextLink = this.paginationNode.querySelector('.gel-pagination__next');
        var nextMore = this.paginationNode.querySelector('.gel-pagination__nextMore');
        var startPageLinkIndex = 0;
        var endPageLinkIndex = this.pageCount;

        if (this.page < startPageLinkIndex + MAX_PAGE_LINKS - 1) {
          endPageLinkIndex = Math.min(this.pageCount, MAX_PAGE_LINKS);
        } else if (this.page > this.pageCount - MAX_PAGE_LINKS + 1) {
          startPageLinkIndex = Math.max(0, this.pageCount - MAX_PAGE_LINKS);
        } else {
          startPageLinkIndex = this.page - Math.floor(MAX_PAGE_LINKS / 2);
          endPageLinkIndex = this.page + Math.ceil(MAX_PAGE_LINKS / 2);
        }

        for (var i = startPageLinkIndex; i < endPageLinkIndex; i++) {
          var newElement = document.createElement('div');
          newElement.innerHTML = "\n            <li class=\"gel-pagination__page\">\n                <a href=\"#".concat(i, "\" tabindex=\"0\" aria-label=\"").concat(this.page === i ? 'Current' : '', " Page ").concat(i + 1, "\" role=\"link\">\n                    ").concat(i + 1, "\n                </a>\n            </li>").trim();

          if (this.page === i) {
            newElement.querySelector("li").classList.add('gel-pagination__disabled', 'gel-pagination__active');
          }

          this.paginationNode.insertBefore(newElement.firstChild, nextMore);
        }

        toggleClass(prevMore, 'd-none', startPageLinkIndex <= 0);
        toggleClass(nextMore, 'd-none', endPageLinkIndex >= this.pageCount);
        toggleClass(prevLink, 'gel-pagination__disabled', this.page <= 0);
        toggleClass(nextLink, 'gel-pagination__disabled', this.page >= this.pageCount - 1);
        this.paginationNode.classList.remove('d-none');
      }
    }, {
      key: "onApplyFilterButtonClicked",
      value: function onApplyFilterButtonClicked() {
        this.update();
        $('.modal-filter').modal('hide');
      }
    }, {
      key: "onPaginationClicked",
      value: function onPaginationClicked(evt) {
        evt.preventDefault();
        var anchor = evt.target.tagName === 'A' ? evt.target : evt.target.parentElement;
        var targetHref = anchor.getAttribute('href');
        var isDisabled = anchor.parentElement.classList.contains('gel-pagination__disabled');

        if (targetHref == null || isDisabled) {
          return;
        }

        this.page = this.getPageToBeShown(targetHref);
        this.renderResult(this.filteredResults, this.page, ITEMS_PER_PAGE);
      }
    }, {
      key: "getPageToBeShown",
      value: function getPageToBeShown(targetHref) {
        switch (targetHref) {
          case '#prev':
            return Math.max(0, this.page - 1);

          case '#next':
            return Math.min(this.pageCount, this.page + 1);

          default:
            return parseInt(targetHref.substr(1));
        }
      }
    }]);

    return ContentAggregator;
  }();

  $(document).ready(function () {
    var componentRoot = document.querySelector('.gel-content-aggregator');

    if (componentRoot) {
      var results = {
        items: []
      };

      try {
        var resultNode = componentRoot.querySelector('[rel="result"]');

        if (resultNode) {
          results = JSON.parse(resultNode.innerText.trim());
          resultNode.parentNode.removeChild(resultNode);
        }
      } catch (_unused) {
        console.error("Failed to to parse content aggregator results");
      }

      new ContentAggregator(componentRoot, results);
    }
  });
});