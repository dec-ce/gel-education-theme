(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jquery", "uikit"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jquery"), require("uikit"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jquery, global.uikit);
    global.mobileNavAccessibility = mod.exports;
  }
})(this, function (_exports, $, _uikit) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  $ = _interopRequireWildcard($);
  _uikit = _interopRequireDefault(_uikit);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */
  var mobileNavAccessibility =
  /*#__PURE__*/
  function () {
    /**
    * Script for Google translate feature
    *
    * @constructor
    *
    * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
    * @param {Object} config - class configuration options. Options vary depending on need
    *
    */
    function mobileNavAccessibility(selector, config) {
      var _this = this;

      _classCallCheck(this, mobileNavAccessibility);

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
      } // Observer pattern for changing aria-expanded attribute from li to child element a


      var attrObserver = this.setAttrObserver(); // Observer for setting class on close button

      var closeLinkObserver = this.setMutationObserver(); // Observer for search button toggle aria attributes

      var searchAttrObserver = this.setSearchMutationObserver(); // Constructor Variables

      this.navElement = $(selector);
      this.offCanvasContainer = $('.uk-offcanvas-bar.edu-offcanvas-bar');
      this.offCanvasContainerJs = document.querySelector('#mobile-nav-container.uk-offcanvas-bar.edu-offcanvas-bar');
      this.mobileNavButton = $('.edu-local-mobile-nav__icon--nav');
      this.closeMenuWrap = $('.edu-local-mobile-nav__close');
      this.closeMenu = $('.edu-local-mobile-nav__close--button');
      this.closeMenuHidden = $('.edu-local-mobile-nav__close--button-hidden');
      this.expandElements = document.querySelector('.edu-local-mobile-nav__li-menu-items.uk-parent');
      this.parentEls = document.querySelectorAll('.edu-local-mobile-nav__li-menu-items.uk-parent');
      this.mobileHead = $('#mobile-nav');
      this.mobileMenuIsActive = false;
      this.focusableItems = $('body a:not(.edu-offcanvas-bar__header):not(.edu-offcanvas-bar__quicklinks-a):not(.edu-local-mobile-nav__li--name-a):not(.edu-local-mobile-nav__plus-icon), body input, body iframe, body button:not(.edu-local-mobile-nav__close--button):not(.edu-local-mobile-nav__close--button-hidden)');
      this.allSRItems = $('body').children(':not(#mobile-nav-wrapper)');
      this.allMenuLinks = $('.uk-offcanvas-bar.edu-offcanvas-bar a');
      this.lastMenuLink = $('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child > a.edu-local-mobile-nav__plus-icon');
      this.lastMenuLinkJs = document.querySelector('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child');
      this.lastMenuHasSubMenu = true;
      this.body = $('body');

      if (!(this.lastMenuLink.length > 0)) {
        this.lastMenuLink = $('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child > .edu-local-mobile-nav__li--name > a.edu-local-mobile-nav__li--name-a');
        this.lastMenuHasSubMenu = false;
      }

      this.headerclass = $('.gel-global-header-container');
      /*coomenting the below code as sws search icon is not used in gel edu
          // get the search icon parent
          //let searchIcon = document.querySelector('#sws-search-icon-mobile')
      
          // variable for aria expanded state
          //this.ariaExpandedState = searchIcon.getAttribute('aria-expanded')
      
          // Add event listener and element for mutation observer
          //searchAttrObserver.observe(searchIcon, {attributes: true})
      */
      // Event listeners

      this.navElement.on('show.uk.offcanvas', function () {
        _this.showOffCanvas();
      });
      this.navElement.on('hide.uk.offcanvas', function () {
        _this.hideOffCanvas();
      });
      this.closeMenu.click(function () {
        UIkit.offcanvas.hide();
      });
      this.closeMenuHidden.click(function () {
        UIkit.offcanvas.hide();
      }); // Subscribe to MutationObserver targeting li.uk-parent elements

      for (var i = 0; i < this.parentEls.length; i++) {
        attrObserver.observe(this.parentEls[i], {
          attributes: true
        });
      }

      closeLinkObserver.observe(this.offCanvasContainerJs, {
        attributes: true
      }); // console.log('End of mobileNavAccessibility constructor')
    }

    _createClass(mobileNavAccessibility, [{
      key: "setMutationObserver",
      value: function setMutationObserver() {
        var _this2 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            console.log("hey" + mutation.attributeName);

            if (mutation.attributeName == 'class') {
              if (mutation.target.classList.contains('uk-offcanvas-bar-show')) {
                _this2.closeMenuWrap.addClass('button-active');
              } else {
                _this2.closeMenuWrap.removeClass('button-active');
              }
            }
          });
        });
      }
    }, {
      key: "setAttrObserver",
      value: function setAttrObserver() {
        var _this3 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'aria-expanded') {
              var target = $(mutation.target)[0];

              _this3.attributeChangeHandler(target);
            }
          });
        });
      }
    }, {
      key: "setSearchMutationObserver",
      value: function setSearchMutationObserver() {
        var _this4 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'aria-expanded') {
              // console.log('mutation.target', mutation.target)
              var target = $(mutation.target)[0]; // console.log('MutationObserver triggered, target', target)

              _this4.searchAttrMutationHandler(target);
            }
          });
        });
      }
    }, {
      key: "closeMenuClassCheck",
      value: function closeMenuClassCheck(bool) {
        var hasClass = this.closeMenuWrap.hasClass('button-active');

        if (bool) {
          if (!hasClass) {
            this.closeMenuWrap.addClass('button-active');
          }
        } else {
          if (hasClass) {
            this.closeMenuWrap.removeClass('button-active');
          }
        }
      }
    }, {
      key: "showOffCanvas",
      value: function showOffCanvas() {
        var _this5 = this;

        this.toggleAriaExpanded(this.mobileNavButton, true);
        this.closeMenuClassCheck(true);
        this.headerclass.addClass('display-hide');
        this.body.addClass("edu-local-mobile-nav__open");
        this.lastMenuLink.on('keydown', function (e) {
          if (e.key === 'Tab' && !e.shiftKey) {
            _this5.moveFocusToHead();
          }
        });
        this.closeMenu.on('keydown', function (e) {
          if (e.key === 'Tab' && e.shiftKey) {
            _this5.lastMenuLink.focus();

            e.stopPropagation();
            setTimeout(function () {
              _this5.lastMenuLink.focus();
            });
          }
        });
      }
    }, {
      key: "hideOffCanvas",
      value: function hideOffCanvas() {
        this.toggleAriaExpanded(this.mobileNavButton, false);
        this.closeMenuClassCheck(false);
        this.lastMenuLink.off();
        this.closeMenu.off();
        this.headerclass.removeClass('display-hide');
        this.body.removeClass("edu-local-mobile-nav__open");
      }
    }, {
      key: "toggleAriaExpanded",
      value: function toggleAriaExpanded(element, bool) {
        $(element).attr('aria-expanded', bool); // If Nav is expanded

        if (bool) {
          // shift focus to the Nav header
          this.mobileMenuIsActive = true;
          this.moveFocusToHead();
          var focused = document.activeElement;
          this.toggleTabindex('-1');
          this.setAriaHiddenLabel(this.mobileMenuIsActive);
        } else {
          // remove focus from the Nav header
          this.mobileMenuIsActive = false;
          this.mobileHead.attr('tabindex', '-1');
          this.toggleTabindex('0');
          this.setAriaHiddenLabel(this.mobileMenuIsActive);
        }
      }
    }, {
      key: "setAriaHiddenLabel",
      value: function setAriaHiddenLabel(bool) {
        if (bool) {
          this.allSRItems.each(function () {
            $(this).attr('aria-hidden', bool);
          });
        } else {
          this.allSRItems.each(function () {
            $(this).removeAttr('aria-hidden');
          });
        }
      }
    }, {
      key: "toggleTabindex",
      value: function toggleTabindex(tabIndex) {
        if (tabIndex === '-1') {
          this.focusableItems.each(function () {
            $(this).attr('tabindex', tabIndex);
          });
        } else {
          this.focusableItems.each(function () {
            $(this).removeAttr('tabindex');
          });
        }
      }
    }, {
      key: "moveFocusToHead",
      value: function moveFocusToHead() {
        this.closeMenu.attr('tabindex', '0');
        this.closeMenu.focus();
      }
    }, {
      key: "attributeChangeHandler",
      value: function attributeChangeHandler(element) {
        // Called everytime the aria-expanded attribute of li.uk-parent changes
        var el = $(element);
        var state = el.attr('aria-expanded'); // make sure the event is valid by checking of the attribute exists

        if (typeof state !== 'undefined') {
          // remove the attribute if it exists
          el.removeAttr('aria-expanded');
          var parentTitle = el.find('.edu-local-mobile-nav__li--name-a').first().text().toString();
          var expandButton = el.children('.edu-local-mobile-nav__plus-icon');
          var currentState = expandButton.attr('aria-expanded');
          var labelString = state === 'true' ? ' - sub menu open' : ' - sub menu closed';
          labelString = parentTitle + labelString; // Add the attribute

          expandButton.attr('aria-expanded', state);
          expandButton.attr('aria-label', labelString);
        }
      }
    }, {
      key: "searchAttrMutationHandler",
      value: function searchAttrMutationHandler(element) {
        // handle removal and addition of aria attributes
        var state = element.getAttribute('aria-expanded');

        if (state !== null) {
          this.ariaExpandedState = state;
        }

        var ariaExpanded = element.getAttribute('aria-expanded');
        var ariaHaspopup = element.getAttribute('aria-haspopup');

        if (ariaExpanded) {
          element.removeAttribute('aria-expanded');
        }

        if (ariaHaspopup) {
          element.removeAttribute('aria-haspopup');
        }

        var expandButton = element.querySelector('.edu-search-icon__button');
        this.ariaExpandedState === 'true' ? expandButton.setAttribute('aria-label', 'Close search') : expandButton.setAttribute('aria-label', 'Open search');
      }
    }]);

    return mobileNavAccessibility;
  }();

  var _default = mobileNavAccessibility;
  _exports["default"] = _default;
});