(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jquery"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jquery"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jquery);
    global.forceLinkOnQuery = mod.exports;
  }
})(this, function (_exports, $) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  $ = _interopRequireWildcard($);

  function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  /**
  * forceLinkOnQuery.js - utility which forces linking when triggered for link elements at specified breakpoints where they have previously been blocked via preventDefault()
  *
  * @since 1.1.12
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */
  var ForceLinkOnQuery =
  /*#__PURE__*/
  function () {
    /**
     *
     * @param {jquery} selector - the element which is modified
     * @param {string} config.media_query - the media query which when triggers forces linking
     *
     */
    function ForceLinkOnQuery(selector, config) {
      _classCallCheck(this, ForceLinkOnQuery);

      this.selector = selector;
      this.config = {
        media_query: "(min-width: 960px)"
      }; // Check if config has been passed to constructor

      if (config) {
        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
      } // set the mediq query


      var media_query = window.matchMedia(this.config.media_query); // Test the media query

      this.testMediaQuery(media_query, selector);
    }
    /**
     * Tests the media query
     *
     * @param {string} mq - the media query string
     */


    _createClass(ForceLinkOnQuery, [{
      key: "testMediaQuery",
      value: function testMediaQuery(mq, selector) {
        if (mq.matches) {
          this.forceLink(selector);
        }
      }
      /**
       * Forces linking on click
       *
       * @param {jquery} selector - the element in question
       */

    }, {
      key: "forceLink",
      value: function forceLink(selector) {
        // check selector is an anchor tag or find the closest one
        if (!$(selector).is('a')) {
          selector = $(selector).closest('a');
        }

        $(selector).on("click", function () {
          window.location = $(this).attr('href');
        });
      }
    }]);

    return ForceLinkOnQuery;
  }();

  var _default = ForceLinkOnQuery;
  _exports["default"] = _default;
});