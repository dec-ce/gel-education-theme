(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jquery", "uikit"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jquery"), require("uikit"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jquery, global.uikit);
    global.ariaToggleMenu = mod.exports;
  }
})(this, function (_exports, $, _uikit) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  $ = _interopRequireWildcard($);
  _uikit = _interopRequireDefault(_uikit);

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */
  var ariaToggleMenu =
  /**
   * Script for Google translate feature
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   *
   */
  function ariaToggleMenu(selector, config) {
    _classCallCheck(this, ariaToggleMenu);

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config);
    }
    /* UIKIT's function for dropdown open event 
      This extra bit of coding is needed so that the aria labels get set correctly in the anchor tag */


    $('[data-uk-dropdown]').on('show.uk.dropdown', function () {
      $(this).children('a').attr("aria-expanded", "true");
      $(this).find('.edu-dropdown-menu').removeAttr("aria-hidden"); //adding the following conditions to fix tab issue in IE11
      //$(this).find('.edu-dropdown-menu').removeAttr('sizset').removeAttr('sizcache');
      //$(this).find('.edu-meganav-container').attr("aria-hidden", "true");
      //$(this).find('.edu-mega-nav-desc').attr("aria-hidden", "true");
      //$(this).find('.edu-meganav-container').focus();

      $(this).find('.edu-dropdown-menu').attr("tabindex", "0");
      removeAriaAttributes();
    });
    /*  UIKIT's function for dropdown close event*/

    $('[data-uk-dropdown]').on('hide.uk.dropdown', function () {
      $(this).children('a').attr("aria-expanded", "false");
      removeAriaAttributes();
    });
    /* Removing unnecessary aria-tags in the li */

    $('.edu-meganav-megalinks').on('keyup', function (e) {
      var code = e.keyCode ? e.keyCode : e.which;

      if (code == 9) {
        $(this).parent().prev('.edu-parent-menu').removeClass('uk-open');
        $(this).parent().prev('.edu-parent-menu').removeAttr("aria-expanded");
        $(this).parent().prev('.edu-parent-menu').removeAttr("aria-haspopup");
        removeAriaAttributes();
      }
    }); //identify the last link of the last submenu so that when the user tabs off the megamenu closes

    $('.edu-mega-menu-title-bar li.edu-parent-menu:last-child').addClass("edu-close-dropdown__lastlist");
    $('.edu-close-dropdown__lastlist li:last-child').addClass("edu-close-dropdown__lastlist-link");
    $('.edu-close-dropdown__lastlist-link').on('keydown', function (e) {
      var code = e.keyCode ? e.keyCode : e.which;

      if (code === 9) {
        $('.edu-close-dropdown__lastlist').removeClass('uk-open');
        $('.edu-close-dropdown__lastlist').removeAttr("aria-expanded");
        $('.edu-close-dropdown__lastlist').removeAttr("aria-haspopup");
        UIkit.dropdown('.uk-dropdown').hide();
        $('uk-dropdown').attr("aria-hidden", "true");
      }
    });
    $("li.uk-parent.edu-parent-menu").mouseover(function () {
      setTimeout(function () {
        $(this).each(function () {
          removeAriaAttributes();
        });
      });
    });

    function removeAriaAttributes() {
      $('li.uk-parent.edu-parent-menu').removeAttr("aria-expanded");
      $('li.uk-parent.edu-parent-menu').removeAttr("aria-haspopup");
    }
  };

  var _default = ariaToggleMenu;
  _exports["default"] = _default;
});