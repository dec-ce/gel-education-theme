(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jquery"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jquery"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jquery);
    global.changeHtmlAttributesOnQuery = mod.exports;
  }
})(this, function (_exports, $) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  $ = _interopRequireWildcard($);

  function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; if (obj != null) { var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  /**
  * ChangeHtmlAttributesOnQuery.js - changes attributes on media query
  *
  * @since 1.1.12b
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */
  var ChangeHtmlAttributesOnQuery =
  /*#__PURE__*/
  function () {
    /**
     *
     * @param {jquery} selector - the element which is modified
     * @param {string} config.media_query - the media query which when triggers removes the tabindex attribute
     * @param {jquery} config.attributes_to_remove - the attributes to remove on the selector, separated by a space
     * @param {object} config.attribute_changes - the attibutes to change on the selector in "name" : "value" pairs
     */
    function ChangeHtmlAttributesOnQuery(selector, config) {
      _classCallCheck(this, ChangeHtmlAttributesOnQuery);

      this.selector = selector;
      this.config = {
        media_query: "(min-width: 960px)",
        attributes_to_remove: "",
        attribute_changes: ""
      }; // Check if config has been passed to constructor

      if (config) {
        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
      } // set the mediq query


      var media_query = window.matchMedia(this.config.media_query); // Test the media query

      this.testMediaQuery(media_query, selector, config);
    }
    /**
     * Tests the media query
     *
     * @param {string} mq - the media query string
     */


    _createClass(ChangeHtmlAttributesOnQuery, [{
      key: "testMediaQuery",
      value: function testMediaQuery(mq, selector, config) {
        if (mq.matches) {
          this.removeAttributes(selector, config.attributes_to_remove);
          this.attributeChanges(selector, config.attribute_changes);
        }
      }
      /**
       * Removes attributes from selector
       *
       * @param {jquery} selector - the element which is modified
       * @param {jquery} removals - the attributes to remove on the selector separated by a space
       */

    }, {
      key: "removeAttributes",
      value: function removeAttributes(selector, removals) {
        $(selector).removeAttr(removals);
      }
      /**
       * Changes attributes on selector
       *
       * @param {jquery} selector - the element which is modified
       * @param {object} changes - the attibutes to change on the selector in "name" : "value" pairs
       */

    }, {
      key: "attributeChanges",
      value: function attributeChanges(selector, changes) {
        $(selector).attr(changes);
      }
    }]);

    return ChangeHtmlAttributesOnQuery;
  }();

  var _default = ChangeHtmlAttributesOnQuery;
  _exports["default"] = _default;
});