mixin general_erm_content
  h1
    |Risk management

  h2
    |What is risk management and why does it matter?

  p
    |Risk management is the process by which an organisation minimises threats and enhances opportunities to help achieve its objectives. It involves much more than compiling a list of everything that can go wrong. It is a proactive process that is fundamental to good business practice.
  p
    |That is why the Department of Education has a risk management framework that involves all areas of the organisation, and why the NSW government requires all its agencies to integrate risk management into their planning.

  h3.uk-h5
    |What is risk?

  p
    |All activities involve risk, which we can define as the effect of uncertainty on the extent to which we will achieve or exceed our objectives.

  p
    |At the department, risks will always continue to emerge because of:

  ul
    li
      |the increasing complexity and scope of our operations
    li
      |the changing nature of our environment and our relationships with stakeholders
    li
      |the increasing need for accountability.

  h3.uk-h5
    |How do we manage risk?

  p
    |We manage risk by anticipating it, understanding it and deciding whether to employ strategies to minimise it. Such strategies could include:

  ul
    li
      |foreseeing opportunities and/or potentially damaging events
    li
      |implementing risk treatment actions
    li
      |providing decision makers with information to effectively assess potential risks.

  p
    |The information on this page is intended to augment the department's #{' '}
    a(href="https://detwww.det.nsw.edu.au/policies/general_man/erm/PD20040036_i.shtml" target="_blank")
      |Risk Management Policy
    | #{' '} and #{' '}
    a(href="https://detwww.det.nsw.edu.au/policies/general_man/erm/implementation_1_PD20040036_i.shtml?level=" target="_blank")
      | Risk Management Guidelines
    |. Using a risk management framework helps us make better decisions and deliver on the department’s outcomes.

  h2
    |When do we need to assess risk?

  ul.erm_icon_label(role="tablist" arialive="polite")
    +erm_icon_label({
      id: "strategic-planning",
      icon_number: "01",
      label_text: "Strategic and business planning and budgeting",
      icon_label_box_text: "Use risk identification and assessment as inputs to the department’s <a href='https://cms.det.nsw.edu.au/enterprise-risk-management/risk-assessment/when-do-we-need-to-assess-risk'>strategic plans and business objectives</a>."
    })

    +erm_icon_label({
      id: "operational-planning",
      icon_number: "02",
      label_text: "Operational planning",
      icon_label_box_text: "Apply the risk management process to plan for day-to-day business or operational activities."
    })

    +erm_icon_label({
      id: "project-planning",
      icon_number: "03",
      label_text: "Project and program planning",
      icon_label_box_text: "Apply the risk management process to project and program planning to identify uncertainties that might affect outcomes."
    })

    +erm_icon_label({
      id: "business-planning",
      icon_number: "04",
      label_text: "Business continuity planning",
      icon_label_box_text: "Apply business continuity planning to enable your critical services to continue during a severe unexpected event."
    })

  h2
    |How do we assess risk?

  ul.erm_icon_label(role="tablist" arialive="polite")
    +erm_icon_label({
      id: "assessing-risk",
      icon_number: "05",
      label_text: "Assessing risk",
      icon_label_box_text: "Identify and describe risks, analysing the likelihood that they will occur as well as consequences if they do occur.",
      extra_classes_single_box: "erm-single-icon-label_box"
    })

  div.erm-icon-caret-down
    i.uk-icon-caret-down(aria-hidden="true")

  ul.erm_icon_label(role="tablist" arialive="polite")

    +erm_icon_label({
      id: "recording-risk",
      icon_number: "05",
      label_text: "Recording risk",
      icon_label_box_text: "Record risks to ensure those responsible can properly manage and report on them.",
      extra_classes_single_box: "erm-single-icon-label_box"
    })

  h2
    |Who has a role in managing risk?

  p
    |Everyone has a role to play in risk management, from the Secretary to the most junior staff member.

  ul.erm_icon_label(role="tablist" arialive="polite")
    +erm_icon_label({
      id: "ministers",
      icon_number: "06",
      label_text: "Ministers",
      icon_label_box_text: "<a href='https://cms.det.nsw.edu.au/enterprise-risk-management/roles-and-responsibilities'>Ministers</a> receive a report of all executive risks related to their responsibilities on a quarterly basis.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "secretary",
      icon_number: "06",
      label_text: "Secretary",
      icon_label_box_text: "The Secretary is ultimately responsible and accountable for risk management across the department.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "executive",
      icon_number: "07",
      label_text: "Executive",
      icon_label_box_text: "Members of the executive are the owners of the department’s extreme and high risks on the executive and division risk registers.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "division-heads",
      icon_number: "08",
      label_text: "Division heads",
      icon_label_box_text: "Division heads are the owners of the department’s extreme and high risks on the executive and division risk registers.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "audit-risk-committee",
      icon_number: "09",
      label_text: "Audit and Risk Committee",
      icon_label_box_text: "The Audit and Risk Committee is an independent source of advice to the Secretary on the department’s governance, risk and control frameworks.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "risk-management-group",
      icon_number: "10",
      label_text: "Enterprise Risk Management Group",
      icon_label_box_text: "The Enterprise Risk Management Group ensures the department adopts risk management in an effective manner across its operations.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "internal-audit",
      icon_number: "06",
      label_text: "Internal Audit",
      icon_label_box_text: "Internal Audit oversees the effectiveness of the risk management framework and reports to the Secretary and the Audit and Risk Committee.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "schools",
      icon_number: "06",
      label_text: "Schools",
      icon_label_box_text: "Schools are responsible for monitoring and reporting risks related to their school environment.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "risk-officer",
      icon_number: "06",
      label_text: "Chief Risk Officer",
      icon_label_box_text: "The Chief Risk Officer is responsible for implementing the risk management framework throughout the department.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "managers",
      icon_number: "08",
      label_text: "Managers",
      icon_label_box_text: "Managers are responsible for managing risks to critical activities that are essential to the department’s operations.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

    +erm_icon_label({
      id: "all-staff",
      icon_number: "09",
      label_text: "All staff",
      icon_label_box_text: "All staff are responsible for communicating new and emerging risks from the internal or external environment.",
      extra_classes_bg_gray: "erm-box-bg-gray"
    })

  h2
    |Risk resources

  ul.erm_icon_label(role="tablist" arialive="polite")
    +erm_icon_label({
      id: "risk-mgnt-policy-guidelines",
      icon_number: "11",
      label_text: "Risk management policy and guidelines",
      icon_label_box_text: "Access full details on the department’s approach to risk management with the risk management policy and guidelines."
    })

    +erm_icon_label({
      id: "other-resources",
      icon_number: "12",
      label_text: "Other resources",
      icon_label_box_text: "Find best practice guides, self-assessment tools, timelines and other tools to implement the risk management framework in your area."
    })

  h2
    |What is business continuity management and why does it matter?

  p
    |Business continuity management helps maintain your critical services during a severe unexpected event.

  ul.erm_icon_label(role="tablist" arialive="polite")
    +erm_icon_label({
      id: "business-continuity-plans",
      icon_number: "13",
      label_text: "Business continuity plans",
      icon_label_box_text: "Maintain your critical services with minimal interruption during a severe unexpected event with the aid of a business continuity plan."
    })

    +erm_icon_label({
      id: "business-continuity-policy-guidelines",
      icon_number: "11",
      label_text: "Business continuity management policy and guidelines",
      icon_label_box_text: "Learn how to manage the effects of severe unexpected events with the business continuity management policy and guidelines."
    })

    +erm_icon_label({
      id: "business-continuity-reference-guide",
      icon_number: "14",
      label_text: "Business continuity management quick reference guide",
      icon_label_box_text: "Get easy access to business continuity advice if you’re already familiar with the guidelines."
    })

  .erm-support
    h2
      |Further support

    p
      |For further support, contact:

    p
      a(href="mailto:risk@det.nsw.edu.au")
        | risk@det.nsw.edu.au
      br
      |(02) 9561 8840