import AggregatorFilter from "./AggregatorFilter";

const ITEMS_PER_PAGE = 10;
const MAX_PAGE_LINKS = 6;

function toggleClass(element, className, state) {
  if(state) {
    element.classList.add(className);
  } else {
    element.classList.remove(className);
  }
}

class ContentAggregator {
  constructor(componentRoot, searchResult) {
    this.root = componentRoot;
    this.searchResult = searchResult;
    this.filteredResults = [];

    const templateNode = componentRoot.querySelector("script[rel='itemTemplate']");
    if (templateNode) {
      this.itemTemplate = templateNode.textContent;
    }

    this.filters = componentRoot.querySelectorAll('.gel-content-aggregator__filter');
    this.resultSummaryNode = componentRoot.querySelector('.results-summary p');
    this.resultItemsNode = componentRoot.querySelector('.gel-result-items');
    this.paginationNode = componentRoot.querySelector('.gel-pagination');
    this.applyFilterBtn = componentRoot.querySelector('.applyFilter');

    this.page = 0;
    this.pageCount = 0;

    this.filters = Array.from(this.filters).map(filterElement => {
      const filterAttributeName = filterElement.getAttribute('data-filter-attribute');
      filterElement.addEventListener('filterChanged', evt => {
        this.filters.forEach(filter => {
          if(filter.attributeName === evt.detail.attributeName && filter.root !== evt.target) {
            filter.setFilterValues(evt.detail.filterValues);
          }
        });
        if(filterElement.getAttribute('data-auto-update') === 'true') {
          this.update();
        }
      });
      return new AggregatorFilter(filterElement, filterAttributeName);
    });
    this.applyFilterBtn.addEventListener('click', this.onApplyFilterButtonClicked.bind(this));
    this.paginationNode.addEventListener('click', this.onPaginationClicked.bind(this));
    this.update();
  }

  update() {
    this.page = 0;
    const isFiltering = this.filters
      .map(filter => filter.isActive())
      .reduce((result, current) => (result || current), false);

    if(isFiltering) {
      this.filteredResults = this.searchResult.items.reduce((result, item) => {
        // Filter objects are duplicated due to having a separate DOM elements for mobile, thus we only need to evaluate
        // half the filters
        for (let i = 0; i < this.filters.length/2; i++) {
          const filter = this.filters[i];
          if (result.indexOf(item) === -1 && filter.shouldInclude(item)) {
            result.push(item);
            break;
          }
        }
        return result;
      }, []);
    } else {
      this.filteredResults = this.searchResult.items;
    }
    this.renderResult(this.filteredResults, this.page, ITEMS_PER_PAGE);
  }

  renderResult(items, page, itemsPerPage) {
    this.resultItemsNode.innerHTML = '';
    const offset = page * itemsPerPage;
    for(let i = offset; i < offset + Math.min(itemsPerPage, items.length - offset); i++) {
      const item = items[i];
      this.renderItem(item);
    }
    this.updatePagination(items, page, itemsPerPage);
    this.renderSummary(this.filteredResults, page);
    this.root.querySelector('.result-title').scrollIntoView(true);
  }

  renderItem(item) {
    if(!this.itemTemplate) {
      return;
    }

    const newElement = document.createElement('div');
    newElement.innerHTML = this.itemTemplate.trim();
    newElement.querySelector('.card-title').innerText = item.title;
    newElement.querySelector('a').setAttribute('href', item.contentPath);
    newElement.querySelector('a').setAttribute('title', item.title);
    newElement.querySelector('a').setAttribute('aria-label', item.title);
    const imageNode = newElement.querySelector('img');
    if(item.imagePath) {
      imageNode.setAttribute('src', item.imagePath);
      imageNode.setAttribute('alt', item.imageAltText);
    } else {
      imageNode.parentNode.removeChild(imageNode); // Because IE doesn't support node.remove()
      newElement.querySelector('.card-body-content').classList.remove('col-md-6');
    }
    if(item.isVideo) {
      const cardType = newElement.querySelector('.card-type');
      cardType.innerHTML = (`<span class="fas fa-play-circle" aria-hidden"></span>
                            <span>Video</span>`);
    }
    if(item.isPodcast) {
      const cardType = newElement.querySelector('.card-type');
      cardType.innerHTML = (`<span class="fas fa-podcast" aria-hidden"></span>
                            <span>Podcast</span>`);
    }
    const itemNode = newElement.firstChild;
    this.resultItemsNode.appendChild(itemNode);
  }

  renderSummary(items, page) {
    const startIndex = page * ITEMS_PER_PAGE + (items.length > 0 ? 1 : 0);
    const endIndex = items.length > 0 ?
      (startIndex + Math.min(items.length, ITEMS_PER_PAGE) - 1) : 0;
    this.resultSummaryNode.innerText = (`Showing ${startIndex} - ${endIndex} of ${items.length} ${items.length > 1 ? 'pages' : 'page'}`);
  }

  updatePagination(items, page, itemsPerPage) {
    this.page = page;
    Array.from(this.paginationNode.querySelectorAll('.gel-pagination__page'))
      .forEach(pageLink => pageLink.parentNode.removeChild(pageLink));

    this.pageCount = Math.ceil(items.length / itemsPerPage);
    if(this.pageCount <= 1) {
      this.paginationNode.classList.add('d-none');
      return;
    }

    const prevLink = this.paginationNode.querySelector('.gel-pagination__prev');
    const prevMore = this.paginationNode.querySelector('.gel-pagination__prevMore');
    const nextLink = this.paginationNode.querySelector('.gel-pagination__next');
    const nextMore = this.paginationNode.querySelector('.gel-pagination__nextMore');

    let startPageLinkIndex = 0;
    let endPageLinkIndex = this.pageCount;
    if(this.page < startPageLinkIndex + MAX_PAGE_LINKS - 1) {
      endPageLinkIndex = Math.min(this.pageCount, MAX_PAGE_LINKS);
    } else if(this.page > (this.pageCount - MAX_PAGE_LINKS + 1)) {
      startPageLinkIndex = Math.max(0, this.pageCount - MAX_PAGE_LINKS);
    } else {
      startPageLinkIndex = this.page - Math.floor((MAX_PAGE_LINKS)/2);
      endPageLinkIndex = this.page + Math.ceil((MAX_PAGE_LINKS)/2);
    }

    for(let i=startPageLinkIndex; i < endPageLinkIndex; i++) {
      const newElement = document.createElement('div');
      newElement.innerHTML = (`
            <li class="gel-pagination__page">
                <a href="#${i}" tabindex="0" aria-label="${this.page === i ? 'Current' : '' } Page ${i + 1}" role="link">
                    ${i + 1}
                </a>
            </li>`.trim());
      if(this.page === i) {
        newElement.querySelector("li")
          .classList.add('gel-pagination__disabled', 'gel-pagination__active');
      }
      this.paginationNode.insertBefore(newElement.firstChild, nextMore);
    }

    toggleClass(prevMore, 'd-none', startPageLinkIndex <= 0);
    toggleClass(nextMore, 'd-none', endPageLinkIndex >= this.pageCount);
    toggleClass(prevLink, 'gel-pagination__disabled', this.page <= 0);
    toggleClass(nextLink, 'gel-pagination__disabled', this.page >= this.pageCount - 1);
    this.paginationNode.classList.remove('d-none');
  }

  onApplyFilterButtonClicked() {
    this.update();
    $('.modal-filter').modal('hide');
  }

  onPaginationClicked(evt) {
    evt.preventDefault();
    const anchor = evt.target.tagName === 'A' ? evt.target : evt.target.parentElement;
    const targetHref = anchor.getAttribute('href');
    const isDisabled = anchor.parentElement.classList.contains('gel-pagination__disabled');
    if(targetHref == null || isDisabled) {
      return;
    }
    this.page = this.getPageToBeShown(targetHref);
    this.renderResult(this.filteredResults, this.page, ITEMS_PER_PAGE);
  }

  getPageToBeShown(targetHref) {
    switch(targetHref) {
      case '#prev' :
        return Math.max(0, this.page - 1);
      case '#next' :
        return Math.min(this.pageCount, this.page + 1);
      default :
        return parseInt(targetHref.substr(1));
    }
  }
}

$(document).ready(function() {
  const componentRoot = document.querySelector('.gel-content-aggregator');
  if(componentRoot) {
    let results = { items: [] };
    try {
      const resultNode = componentRoot.querySelector('[rel="result"]');
      if(resultNode) {
        results = JSON.parse(resultNode.innerText.trim());
        resultNode.parentNode.removeChild(resultNode);
      }
    } catch {
      console.error("Failed to to parse content aggregator results");
    }
    new ContentAggregator(componentRoot, results);
  }
});
