class AggregatorFilter {

  constructor(root, attributeName) {
    this.root = root;
    this.attributeName = attributeName;
    this.filterWrapper = root.querySelector('.gel-content-aggregator__collapsible-wrapper.filter-content');
    this.optionsWrapper = root.querySelector('.gel-content-aggregator__collapsible-wrapper.filter-options');
    this.filterValues = [];

    if (this.optionsWrapper) {
      this.calculateOptionsHeight();
    }

    if (this.filterWrapper) {
      this.filterContent = this.filterWrapper.querySelector('.wrapper-content');
      this.expand(this.root.classList.contains('expanded'));

      root.querySelector('.filter-title').addEventListener('click', evt => {
        evt.preventDefault();
        this.expand(!this.root.classList.contains('expanded'));
      });
    }

    if (root.querySelector('.more-options')) {
      root.querySelector('.more-options').addEventListener('click', evt => {
        evt.preventDefault();
        const optionsContent = this.optionsWrapper.querySelector('.wrapper-content');
        let filterHeight = this.filterWrapper.querySelector('.wrapper-content').clientHeight;

        this.optionsWrapper.classList.add('expanded');
        this.optionsWrapper.setAttribute('style', `max-height: ${optionsContent.clientHeight}px`);
        filterHeight = filterHeight + (optionsContent.clientHeight - this.collapsedOptionsHeight);
        this.filterWrapper.setAttribute('style', `max-height: ${filterHeight}px`);
      });
    }

    if (root.querySelector('.less-options')) {
      root.querySelector('.less-options').addEventListener('click', evt => {
        evt.preventDefault();
        this.optionsWrapper.setAttribute('style', `max-height:${this.collapsedOptionsHeight}px`);
        this.optionsWrapper.classList.remove('expanded');
      });
    }

    const filterOptions = root.querySelector('.filter-options');
    if(filterOptions) {
      filterOptions.addEventListener('change', evt => {
        const filterValue = evt.target.parentElement.querySelector('input').getAttribute('value');
        const valueIndex = this.filterValues.indexOf(filterValue);
        if(valueIndex  > -1) {
          this.filterValues.splice(valueIndex, 1);
        } else {
          this.filterValues.push(filterValue);
        }
        this.root.dispatchEvent(new CustomEvent('filterChanged',
          {
            detail: {
              attributeName: this.attributeName,
              filterValues: this.filterValues
            }
          }));
      });
    }
  }

  calculateOptionsHeight() {
    this.collapsedOptionsHeight = 0;
    const options = this.root.querySelectorAll('.option');
    for(let i=0; i < Math.min(5, options.length); i++) {
      this.collapsedOptionsHeight += (options[i].clientHeight + 8);
    }
    this.optionsWrapper.setAttribute('style', `max-height:${this.collapsedOptionsHeight}px`);

    return this.collapsedOptionsHeight;
  }

  isExpanded() {
    return this.root.classList.contains('expanded');
  }

  expand(expand) {
    if (!expand) {
      this.filterWrapper.setAttribute('style', 'max-height:0px');
      this.root.classList.remove('expanded');
    } else {
      this.filterWrapper.setAttribute('style', `max-height: ${this.filterContent.clientHeight}px`);
      this.root.classList.add('expanded');
    }
  }

  setFilterValues(filterValues) {
    Array.from(this.root.querySelectorAll('input[type="checkbox"]')).forEach(checkbox => {
      checkbox.checked = filterValues.indexOf(checkbox.value) > -1;
    });
    this.filterValues = filterValues;
  }

  shouldInclude(item) {
    const attributeValue = item[this.attributeName];
    if(Array.isArray(attributeValue)) {
      return item[this.attributeName].reduce((result, tag) => result || this.filterValues.indexOf(tag) > -1, false);
    } else {
      return this.filterValues.indexOf(item[this.attributeName]) > -1;
    }
  }

  isActive() {
    return this.filterValues.length > 0;
  }
}

export default AggregatorFilter;
