"use strict"
import * as $ from "jquery"

/**
* forceLinkOnQuery.js - utility which forces linking when triggered for link elements at specified breakpoints where they have previously been blocked via preventDefault()
*
* @since 1.1.12
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class ForceLinkOnQuery {

  /**
   *
   * @param {jquery} selector - the element which is modified
   * @param {string} config.media_query - the media query which when triggers forces linking
   *
   */
  constructor(selector, config) {

    this.selector = selector

    this.config = {
      media_query: "(min-width: 960px)"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // set the mediq query
    var media_query = window.matchMedia(this.config.media_query)
    // Test the media query
    this.testMediaQuery(media_query, selector)
  }

  /**
   * Tests the media query
   *
   * @param {string} mq - the media query string
   */
  testMediaQuery(mq, selector) {
    if (mq.matches) {
      this.forceLink(selector)
    }
  }

  /**
   * Forces linking on click
   *
   * @param {jquery} selector - the element in question
   */
  forceLink(selector) {

    // check selector is an anchor tag or find the closest one
    if (!$(selector).is('a')) {
      selector = $(selector).closest('a')
    }

    $(selector).on("click", function() {
      window.location = $(this).attr('href')
    })
  }

}

export default ForceLinkOnQuery