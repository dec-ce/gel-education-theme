"use strict"
import * as $ from "jquery"
import UI from "uikit"
/**
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class mobileNavAccessibility {


   /**
   * Script for Google translate feature
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   *
   */

  constructor(selector, config) {

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Observer pattern for changing aria-expanded attribute from li to child element a
    const attrObserver = this.setAttrObserver()
    // Observer for setting class on close button
    const closeLinkObserver = this.setMutationObserver()
    // Observer for search button toggle aria attributes
    const searchAttrObserver = this.setSearchMutationObserver()

    // Constructor Variables
    this.navElement = $(selector)
    this.offCanvasContainer = $('.uk-offcanvas-bar.edu-offcanvas-bar')
    this.offCanvasContainerJs = document.querySelector('#mobile-nav-container.uk-offcanvas-bar.edu-offcanvas-bar')
    this.mobileNavButton = $('.edu-local-mobile-nav__icon--nav')
    this.closeMenuWrap = $('.edu-local-mobile-nav__close')
    this.closeMenu = $('.edu-local-mobile-nav__close--button')
    this.closeMenuHidden = $('.edu-local-mobile-nav__close--button-hidden')
    this.expandElements = document.querySelector('.edu-local-mobile-nav__li-menu-items.uk-parent')
    this.parentEls = document.querySelectorAll('.edu-local-mobile-nav__li-menu-items.uk-parent')
    this.mobileHead = $('#mobile-nav')
    this.mobileMenuIsActive = false
    this.focusableItems = $('body a:not(.edu-offcanvas-bar__header):not(.edu-offcanvas-bar__quicklinks-a):not(.edu-local-mobile-nav__li--name-a):not(.edu-local-mobile-nav__plus-icon), body input, body iframe, body button:not(.edu-local-mobile-nav__close--button):not(.edu-local-mobile-nav__close--button-hidden)')
    this.allSRItems = $('body').children(':not(#mobile-nav-wrapper)');
    this.allMenuLinks = $('.uk-offcanvas-bar.edu-offcanvas-bar a')
    this.lastMenuLink = $('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child > a.edu-local-mobile-nav__plus-icon')
    this.lastMenuLinkJs = document.querySelector('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child')
    this.lastMenuHasSubMenu = true
    this.body = $('body');
    if (!(this.lastMenuLink.length > 0)) {
      this.lastMenuLink = $('.edu-offcanvas-bar__main-unordered-list > li.edu-local-mobile-nav__li-menu-items:last-child > .edu-local-mobile-nav__li--name > a.edu-local-mobile-nav__li--name-a')
      this.lastMenuHasSubMenu = false
    }
    this.headerclass=$('.gel-global-header-container')
/*coomenting the below code as sws search icon is not used in gel edu
    // get the search icon parent
    //let searchIcon = document.querySelector('#sws-search-icon-mobile')

    // variable for aria expanded state
    //this.ariaExpandedState = searchIcon.getAttribute('aria-expanded')

    // Add event listener and element for mutation observer
    //searchAttrObserver.observe(searchIcon, {attributes: true})
*/
    // Event listeners
    this.navElement.on('show.uk.offcanvas', () => {
      this.showOffCanvas() })
    this.navElement.on('hide.uk.offcanvas', () => { this.hideOffCanvas() })
    this.closeMenu.click(() => { UIkit.offcanvas.hide() })
    this.closeMenuHidden.click(() => { UIkit.offcanvas.hide() })

    // Subscribe to MutationObserver targeting li.uk-parent elements
    for (let i = 0; i < this.parentEls.length; i++) {
      attrObserver.observe(this.parentEls[i], {attributes: true})
    }

    closeLinkObserver.observe(this.offCanvasContainerJs, {attributes: true})
    // console.log('End of mobileNavAccessibility constructor')
  }

  setMutationObserver () {
    return new MutationObserver ((mutations) => {
      mutations.forEach((mutation) => {
        console.log("hey"+ mutation.attributeName);
        if (mutation.attributeName == 'class') {
          if (mutation.target.classList.contains('uk-offcanvas-bar-show')) {
            this.closeMenuWrap.addClass('button-active')
          } else {
            this.closeMenuWrap.removeClass('button-active')
          }
        }
      })
    })
  }

  setAttrObserver () {
    return new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.attributeName == 'aria-expanded') {
          let target = $(mutation.target)[0]
          this.attributeChangeHandler(target)
        }
      })
    })
  }

  setSearchMutationObserver () {
    return new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.attributeName == 'aria-expanded') {
          // console.log('mutation.target', mutation.target)
          let target = $(mutation.target)[0]
          // console.log('MutationObserver triggered, target', target)
          this.searchAttrMutationHandler(target)
        }
      })
    })
  }

  closeMenuClassCheck (bool) {
    let hasClass = this.closeMenuWrap.hasClass('button-active')
    if (bool) {
      if (!hasClass) {
        this.closeMenuWrap.addClass('button-active')
      }
    } else {
      if (hasClass) {
        this.closeMenuWrap.removeClass('button-active')
      }
    }
  }

  showOffCanvas () {
    this.toggleAriaExpanded(this.mobileNavButton, true)
    this.closeMenuClassCheck(true)
    this.headerclass.addClass('display-hide')
    this.body.addClass("edu-local-mobile-nav__open")
    this.lastMenuLink.on('keydown', (e) => {
      if (e.key === 'Tab' && !e.shiftKey) {
        this.moveFocusToHead()
      }
    })

    this.closeMenu.on('keydown', (e) => {
      if (e.key === 'Tab' && e.shiftKey) {
        this.lastMenuLink.focus()
        e.stopPropagation()
        setTimeout(() => {
          this.lastMenuLink.focus()
        })
      }
    })
  }

  hideOffCanvas () {
    this.toggleAriaExpanded(this.mobileNavButton, false)
    this.closeMenuClassCheck(false)
    this.lastMenuLink.off()
    this.closeMenu.off()
    this.headerclass.removeClass('display-hide')
    this.body.removeClass("edu-local-mobile-nav__open")
  }

  toggleAriaExpanded (element, bool) {
    $(element).attr('aria-expanded', bool)

    // If Nav is expanded
    if (bool) {
      // shift focus to the Nav header
      this.mobileMenuIsActive = true
      this.moveFocusToHead()
      var focused = document.activeElement
      this.toggleTabindex('-1')
      this.setAriaHiddenLabel(this.mobileMenuIsActive)
    } else {
      // remove focus from the Nav header
      this.mobileMenuIsActive = false
      this.mobileHead.attr('tabindex', '-1')
      this.toggleTabindex('0')
      this.setAriaHiddenLabel(this.mobileMenuIsActive)
    }
  }

  setAriaHiddenLabel (bool) {
    if (bool) {
      this.allSRItems.each(function () {
        $(this).attr('aria-hidden', bool)
      })
    } else {
      this.allSRItems.each(function () {
        $(this).removeAttr('aria-hidden')
      })
    }
  }

  toggleTabindex (tabIndex) {
    if(tabIndex === '-1') {
      this.focusableItems.each(function () {
        $(this).attr('tabindex', tabIndex)
      })
    } else {
      this.focusableItems.each(function () {
        $(this).removeAttr('tabindex')
      })
    }
  }

  moveFocusToHead () {
    this.closeMenu.attr('tabindex', '0')
    this.closeMenu.focus()
  }

  attributeChangeHandler (element) {
    // Called everytime the aria-expanded attribute of li.uk-parent changes
    let el = $(element)
    let state = el.attr('aria-expanded')
    // make sure the event is valid by checking of the attribute exists
    if (typeof state !== 'undefined') {
      // remove the attribute if it exists
      el.removeAttr('aria-expanded')
      let parentTitle = el.find('.edu-local-mobile-nav__li--name-a').first().text().toString()
      let expandButton = el.children('.edu-local-mobile-nav__plus-icon')
      let currentState = expandButton.attr('aria-expanded')
      let labelString = state === 'true' ? ' - sub menu open' : ' - sub menu closed'
      labelString = parentTitle + labelString

      // Add the attribute
      expandButton.attr('aria-expanded', state)
      expandButton.attr('aria-label', labelString)
    }
  }

  searchAttrMutationHandler (element) {
      // handle removal and addition of aria attributes
      let state = element.getAttribute('aria-expanded')
      if (state !== null) { this.ariaExpandedState = state }

      let ariaExpanded = element.getAttribute('aria-expanded')
      let ariaHaspopup = element.getAttribute('aria-haspopup')

      if (ariaExpanded) { element.removeAttribute('aria-expanded') }
      if (ariaHaspopup) { element.removeAttribute('aria-haspopup') }

      let expandButton = element.querySelector('.edu-search-icon__button')

      this.ariaExpandedState === 'true' ?
        expandButton.setAttribute('aria-label', 'Close search') :
        expandButton.setAttribute('aria-label', 'Open search')
  }
}

export default mobileNavAccessibility
