"use strict"
import * as $ from "jquery"

/**
* ChangeHtmlAttributesOnQuery.js - changes attributes on media query
*
* @since 1.1.12b
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class ChangeHtmlAttributesOnQuery {

  /**
   *
   * @param {jquery} selector - the element which is modified
   * @param {string} config.media_query - the media query which when triggers removes the tabindex attribute
   * @param {jquery} config.attributes_to_remove - the attributes to remove on the selector, separated by a space
   * @param {object} config.attribute_changes - the attibutes to change on the selector in "name" : "value" pairs
   */
  constructor(selector, config) {

    this.selector = selector

    this.config = {
      media_query: "(min-width: 960px)",
      attributes_to_remove: "",
      attribute_changes: ""
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // set the mediq query
    var media_query = window.matchMedia(this.config.media_query)
    // Test the media query
    this.testMediaQuery(media_query, selector, config)
  }

  /**
   * Tests the media query
   *
   * @param {string} mq - the media query string
   */
  testMediaQuery(mq, selector, config) {
    if (mq.matches) {
      this.removeAttributes(selector, config.attributes_to_remove)
      this.attributeChanges(selector, config.attribute_changes)
    }
  }

  /**
   * Removes attributes from selector
   *
   * @param {jquery} selector - the element which is modified
   * @param {jquery} removals - the attributes to remove on the selector separated by a space
   */
  removeAttributes(selector, removals) {
    $(selector).removeAttr(removals)
  }

  /**
   * Changes attributes on selector
   *
   * @param {jquery} selector - the element which is modified
   * @param {object} changes - the attibutes to change on the selector in "name" : "value" pairs
   */
  attributeChanges(selector, changes) {
    $(selector).attr(changes)
  }
}


export default ChangeHtmlAttributesOnQuery