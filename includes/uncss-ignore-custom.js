"use strict"
module.exports = function() {

  var uncssIgnoreCustom = [
    // add your custom rules here
    /(\.gef-banner.+)/,
    /(\.gef-external-link)/,
    /(\.gef-global-header--alt)/,
    /(\.gef-anchor-box.*)/,
    /(\.gef-notice-ribbon.*)/,
    /(\.gsa-gef-search.*)/,
    /(\.gef-drawer.*)/,
    /(\.gef-datepicker.*)/,
    /(\.gef-home-mobile-breadcrumb.*)/,
    /(\.gef-date-picker.*)/,
    /\[data-ng-cloak\]/,
    /\[ng:cloak\]/,
    /\[ng-cloak\]/,
    /\[tabindex="0"]/,
    /(\.policy-manual-fix.*)/,
    /(\.policy-enquires.*)/,
    /(\.policy-print.*)/,
    /(\.policy-list-items.*)/,
    /(\.policy-callout-box.*)/,
    /(\.gef-maintenance-ribbon__message.*)/,
    /(\.gef-section-footer_variant.*)/,
    /(\.invisible)/,
    /(\.rotate)/,
    /(\.sws-local-mobile-nav)/,
    /(\.display-hide)/,
    /(\#google_translate_element)/,
    /(\.maintenanceRibbon_auto)/,
    /(\.edu-mobilemenu-search)/,
    /(\.edu-local-mobile-nav__open*)/,
    /(\.gef-featured-teaser_highlight.*)/,
    /(\.gef-social-media-post.*)/,
    /(\.edu-social-media-post-block.*)/,
    /(\.advanced-filter.*)/,
    /(\.gef-LD-index.*)/,
    /(\.gef-anchor-index.*)/,
    /(\.back_.*)/,
    /(\.gef-skiplink__link.*)/,
    /(\.button-active.*)/,
    /(\.anchor-level1.*)/,
    /(\.gef-secondary-link-list)/,
    /(\.edu-footerimage-wrapper.*)/,
    /(\.edu-banner.*)/,
    /(\.edu-highlight-features.*)/,
    /(\.gef-form-validate)/,
    /(\.livefyre*)/,
    /(\.gef-form-validate)/,
    /(\.category-.*)/  
  ]

  return uncssIgnoreCustom;

}
