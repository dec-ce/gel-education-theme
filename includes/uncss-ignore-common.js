module.exports = function() {

  var uncssIgnoreCommon = [
    /(\.gef-tabs.+)/,
    /(\.gef-tag-list.*)/,
    /(.+\.active.*)/,
    /(.+\.uk-active.*)/,
    /(.+\[aria-expanded.*)/,
    /(.+\[aria-hidden.*)/,
    /(.+iframe.*)/,
    /(.+placeholder.*)/,
    /(\.gef-ie9.+)/,
    /(\.gef-btt.+)/,
    /(\.gef-pseudo-link)/,
    /(\.gef-breadcrumbs.+)/,
    /(\.gef-pagination.+)/,
    /(\.call-out)/,
    /(\.gef-linkgroup-list.+)/,
    /(\.gef-tile-list.+)/,
    /(\.gef-search.+)/,
    /(\.gef-external-link)/,
    /(\.uk-dropdown.*)/,
    /(\.uk-icon-*)/,
    /(\.uk-h6)/,
    /uk-width-*/,
    /uk-push-*/,
    /uk-pull-*/,
    /(\.uk-datepicker.*)/,
    /\[x-ng-cloak\]/,
    /(\.ng-cloak)/,
    /(\.x-ng-cloak)/,
    /(.+\[data-scroll-passed.*)/,
    /(\.uk-open.*)/,
    /\.uk-modal.*/
  ]

  return uncssIgnoreCommon;

}
