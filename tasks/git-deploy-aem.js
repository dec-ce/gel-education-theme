"use strict"

module.exports = function(gulp, plugins, options, runSequence) {

  // Pull first to catch any updates
  gulp.task("aem-git-pull", function(cb) {
    console.log("repository", options.deployment.repository.root)
    return plugins.git.pull('origin', 'master', {cwd: options.deployment.repository.root}, function (err) {
      if (err) {
        throw err
      } else {
        runSequence('aem-git-deploy', cb)
      }
    });
  });

  // Copy the files
  gulp.task("aem-git-cp", function() {
    console.log("in aem git cp", options.deployment.repository.deploymentLocation)
    console.log("from ", options.deployment.source)
    return gulp.src(options.deployment.source)
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation), function(err) {
      if (err) {
         console.log("error", err)
         throw err
      } else {
         console.log('Copied across');
      }
    })
  })

  // Add any new files to the repo
  gulp.task("aem-git-add", ["aem-git-cp"], function() {
    console.log("in aem-git-add")
    return gulp.src("./.", {cwd: options.deployment.repository.root})
    .pipe(plugins.git.add({cwd: options.deployment.repository.root}, function(err) {
     if (err) {
        console.log("error", err)
        throw err
     } else {
        console.log('Added new files!');
     }
    }))
  });

  // Automated commit message
  gulp.task("aem-git-commit", ["aem-git-add"], function() {
    console.log("in aem git commit")
    return gulp.src("./", {cwd: options.deployment.repository.root})
    .pipe(plugins.git.commit("Automated deployment to gel " + options.deployment.environment + " repository", {cwd: options.deployment.repository.root}, function(err) {
      this.emit("end")
      console.log("The " + options.deployment.environment + " environment is already up to date. Nothing to be added :D")
    })
    )
  })

  // Push that code.
  gulp.task("aem-git-deploy", ["aem-git-commit"], function(cb) {
    console.log("options", options.deployment.args)
    return plugins.git.push("origin", "master", options.deployment.args, function(err) {
    if (err) {
      throw err
    } else {
      console.log("You've successfully updated the " + options.siteName + " " + options.deployment.environment + " environment! Give yourself a pat on the back :) ")
        //gulp.src(__filename)
        //  .pipe(plugins.open({uri: "https://bitbucket.org/dec-ce/edu-to-aem-frontend" + options.siteName.replace(/\s+/g, "-").toLowerCase() + "/" + options.deployment.environment + "/"}))
    }
    })
  })
}
