"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function(callback) {
    const doeSearchSource = "doe-search/build/static/";
    const doeSearchSourceJs = doeSearchSource + "js/";
    const doeSearchSourceCss = doeSearchSource + "css/";
    const doeSearchDestination = "apps/doe-search";

    gulp.src(options.paths.nodeModules + doeSearchSourceJs + "main.search.chunk.js")
      .pipe(plugins.rename("main.search.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + doeSearchDestination))

    gulp.src(options.paths.nodeModules + doeSearchSourceJs + "runtime~main.search.js")
      .pipe(plugins.rename("runtime.search.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + doeSearchDestination))

    gulp.src(options.paths.nodeModules + doeSearchSourceJs + "vendors~main.search.chunk.js")
      .pipe(plugins.rename("vendors.search.js"))
      .pipe(gulp.dest(options.paths.dist.assets.js + doeSearchDestination))

    gulp.src(options.paths.nodeModules + doeSearchSourceCss + "main.search.chunk.css")
      .pipe(plugins.rename("doe-search.css"))
      .pipe(gulp.dest(options.paths.dist.assets.css))

    callback()
  }
}