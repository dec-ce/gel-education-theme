"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    // Fonts found in bower_components/gel/src/assets/fonts
    return gulp.src(options.paths.gel + "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}")
      .pipe(gulp.dest(options.paths.dist.assets.fonts))
      .on("error", errorHandler)
  }
}
