"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {
    return gulp.src([options.paths.gel + "src/assets/js/**/*.{js,es6,es7,es}", "!" + options.paths.gel + "src/assets/js/config/siteConfig.js", options.paths.dev.assets.js])

       // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // only change the files that have changed
      .pipe(plugins.cached('babelling'))


      // .pipe(plugins.babel({
      //   modules: "amd"
      // }))

      .pipe(
        plugins.babel({
          presets: [
            [
              '@babel/env',
              {
                modules: 'umd',
              }
            ]
          ],
          plugins: [
            "@babel/plugin-proposal-class-properties"
          ]
        })
      )

      .pipe(plugins.if(options.compress, plugins.uglify()))

      .pipe(gulp.dest(options.paths.dist.assets.js))
  }
}
